minetest.register_chatcommand("fix_treemarks", {
 params = "<radius> or help",
 description = "Restart tree regrowth timers",
 privs = {server=true},
 func = function(name, param)
    if minetest.check_player_privs(name, {server = true}) == false then
       return false, "You need server privileges to use this command."
    end
    --check valid
    local number = tonumber(param)
    if param == "help" or param == "" or number == nil then
       return "Checks all tree markers in <radius> and restarts them"
    end
    if number > 100 or number < 1 then
       return "<radius> should be between 1 and 100"
    end
    local pos = minetest.get_player_by_name(name):get_pos()
    local minp = vector.round(vector.subtract(pos, number))
    local maxp = vector.round(vector.add(pos, number))
    local tmarks = minetest.find_nodes_in_area(minp, maxp,
					       "nodes_nature:tree_mark")
    for tpos = 1, #tmarks do
       local timer = minetest.get_node_timer(tmarks[tpos])
       if timer:is_started() == false then
	  local meta = minetest.get_meta(tmarks[tpos])
	  local saved_name = meta:get_string("saved_name")
	  if saved_name == "" then -- invalid mark from schematics
	     minetest.remove_node(tmarks[tpos]) -- so kill it
	  else
	     timer:start(2400 + math.random(1, 1200))
	  end
       end
    end
 end
})
